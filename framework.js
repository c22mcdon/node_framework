let srv = require("./server.js");
let framework = function(opt) {
  var me = this;
  this.defaults = (opt != null && opt.defaults != null) ? opt.defaults : {
    pipeline: [
      [
        {
          action: require("./preprocessors/security.js").action,
          method: ".*",
          type: ".*"
        },
        {
          action: require("./preprocessors/request.js").action,
          method: ".*",
          type: ".*"
        },
        {
          action: require("./preprocessors/post.js").action,
          method: "post",
          type: ".*"
        }
      ],
      [{
        action: require("./parsers/x-www-form-urlencoded.js").action,
        method: "post",
        type: "form"
      }],
      [], [{
        action: require("./postprocessors/write.js").action,
        method: ".*",
        type: ".*"
      }]
    ]
  };

  this.server = {
    add: (name, options) => {
      let s = new srv(name, options);
      me.defaults.pipeline.forEach(
          (pipe, index) => pipe.forEach(
              opts => s.addHTTP(
                  {
                    index,
                    action: opts.action,
                  },
                  opts.method, opts.type)));
      return s;
    },
    get: (name) => srv.get(name)
  };
};
framework.get = {
  server: (name) => srv.get(name)
};

module.exports = framework;