module.exports = {
  MIMETypes: {

    binary: "application/octet-stream",
    form: "application/x-www-form-urlencoded",
    json: "application/json",

    pdf: "application/pdf",

    css: "text/css",
    scss: "text/css",
    html: "text/html",
    htm: "text/html",
    text: "text/plain",
    js: "text/javascript",

    png: "image/png",
    jpg: "image/jpeg",
    jpeg: "image/jpeg",
    bmp: "image/bmp",
    gif: "image/gif",
    svg: "image/svg+xml",

    ico: "ico",

    woff2: "font/woff2",
    woff: "font/woff",
    eot: "font/eot",
    ttf: "font/ttf"
  },
  Statuses: {
    Ok: {Code: 200, Message: "OK"},
    NotFound: {Code: 404, Message: "Not Found"},
    InternalError: {Code: 500, Message: "Internal Server Error"}
  }
};