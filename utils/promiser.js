module.exports = {
  promisified: [],
  promisifyNode: function(module) {
    if (this.promisified.indexOf(module) >= 0)
      return module;
    else
      this.promisified.push(module);
    let protoKeys = Object.keys(module.__proto__);
    Object.keys(module).forEach((key) => {
      if (typeof module[key] == "function") {
        module[key + "Async"] = function(...args) {
          return new Promise(function(resolve, reject) {
            args.push(function(ex, data) {
              return ex != null || ex instanceof Error ? reject(ex) :
                                                         resolve(data);
            });
            module[key].apply(module, args);
          });
        };
      };
    });
    protoKeys.forEach(key => {
      if (typeof module.__proto__[key] == "function") {
        module.__proto__[key + "Async"] = function(...args) {
          return new Promise(function(resolve, reject) {
            args.push(function(ex, data) {
              return ex != null || ex instanceof Error ? reject(ex) :
                                                         resolve(data);
            });
            module.__proto__[key].apply(module, args);
          });
        };
      };
    });

    return module;
  },
  promisifySyncs: function(module) {
    Object.keys(module).forEach((key) => {
      if (typeof module[key] == "function") {
        module[key + "Async"] = function(...args) {
          return new Promise(function(resolve, reject) {
            resolve(module[key].apply(module, args));
          });
        };
      };
    });
    return module;
  }
};