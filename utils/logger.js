var promiser = require("./promiser.js");
var fs = promiser.promisifyNode(require("fs"));

module.exports = function(logDir) {

  var root = logDir;
  try {
    fs.accessSync(root);
  } catch (ex) {
    fs.mkdirSync(root);
  }

  this.log = (err) => {
    let message = err instanceof Error ?
        err.name + "\n" + err.message + "\n" + err.stack :
        err;
    let file = root + new Date().toDateString();
    // fire and forget:
    fs.appendFileAsync(
        file,
        "\n----\n" + new Date().toTimeString() + "\n\n" + message + "\n----\n",
        "utf8");
  };
};