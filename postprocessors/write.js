const types = require("../constants.js").MIMETypes;

module.exports = {
  action: async function(req, res, result) {

    // Write cookies:
    // todo: untested!
    // todo: make sure multiple cookies can be set!
    if (res.cookies != null && !res.cookies.empty()) {
      res.cookies.names.forEach(
          (name) => res.write(`Set-Cookie: ${res.cookies.write(name)}`));
    };

    // Write content headers:
    if (result != null) {
      if (result.length != null)
        res.setHeader("Content-Length", result.length);
      else if (this.stat != null)
        res.setHeader("Content-Length", this.size);
    };
    let encoding = "binary";
    if (this.type && types[this.type] != null) {
      if (types[this.type].match(/^text/)) encoding = "utf8";
      if (result != null && result.length > 0)
        res.setHeader("Content-Type", types[this.type]);
    };
    // Write body:
    if (typeof result == "object") result = JSON.stringify(result);
    if (result) res.write(result, encoding);
  }
};