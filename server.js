const http = require("http");
const constants = require("./constants.js");
const promiser = require("./utils/promiser.js");
const fs = promiser.promisifyNode(require("fs"));

let private = {};
private.servers = {};

let server = function(serverName, options) {

  console.log(
      `Creating server with options: ${ options != null ? JSON.stringify(options) : "" }\n`);

  const me = this;

  this.serverName = serverName;
  private.servers[serverName] = this;

  if (options != null) {
    this.extensionAliases = options.extensionAliases || {};

    // Run once when server is created:
    this.initializers = options.initializers || [];

    // Each index is a list of processors.
    // Process low to high
    this.pipeline = [];

    // Additional objects accessible via this server:
    this.componentDirectories = options.componentDirectories || [];

    // File structure settings:
    this.path = options.path || {root: __dirname};

    // Additional settings:
    this.settings = options.settings || {};
  }

  this.logger = options.logger;

  this.components = {};

  this.get = {
    component: (name) => me.components[name],
    processor: (name) => require(`./processors/${name}.js`),
  };

  let server = http.createServer(async(request, response) => {

    try {
      const method = request.method.toLowerCase();
      const dt = private.determineType(me, request);
      let context = {
        extensionAliases: me.extensionAliases,  // actual type <- virtual type
        method: method,
        type: dt.type,
        mime: dt.mime,
        results: [],
        settings: me.settings
      };
      request.type = dt.type;

      let result = null;
      for (let pipeLevel of this.pipeline)
        for (let pipe of pipeLevel)
          if (typeof pipe.condition == "function" && !pipe.condition(request))
            continue;
          else
            result = await pipe.action.call(context, request, response, result);

      if (me.logger != null)
        me.logger.log(
            "REQUEST" + JSON.stringify(request.url) + "\n" +
            JSON.stringify(request.headers) + "\n" +
            JSON.stringify(request.accept) + "\n" +
            JSON.stringify(request.cookies) + "\n" + JSON.stringify(context));

      response.statusCode = constants.Statuses.Ok.Code;
      response.statusMessage = constants.Statuses.Ok.Message;
    } catch (ex) {
      if (me.logger != null) me.logger.log(ex);

      response.statusCode =
          ex.statusCode || constants.Statuses.InternalError.Code;
      response.statusMessage = ex.statusMessage || ex.message ||
          constants.Statuses.InternalError.Message;
    };
    response.end();
  });

  server.on("uncaughtException", function(err) {
    if (me.logger) me.logger.log(err);
  });

  this.listen = function(port) {
    private.initialize(this);
    server.listen(port);
    console.log("Started Server on:" + port);
  };

  this.add = (options) => {
    const index = options.index;

    if (!Array.isArray(me.pipeline[index])) me.pipeline[index] = new Array();

    me.pipeline[index].push(
        {action: options.action, condition: options.condition});
  };

  this.addHTTP = (options, method, type) => {
    options.condition = (req) =>
        req.method.match(method) && req.type.match(type);
    return me.add(options);
  };
  this.addComponent = (name, component) => { me.components[name] = component; };

};

server.get = (name) => private.servers[name];

private.initialize = function(server) {
  if (server.initializers == null || server.initializers.length == 0) return;

  server.initializers.forEach(directory => {
    console.log(`Initializing with directory: ${directory}`);
    fs.readdirSync(directory).forEach(file => {
      let path = directory + file;
      console.log(`Initializing: ${path}`);
      require(path);
    });
  });
};

private.pipe = async function(server, pipeline, context, vars) {
  let method = context.method;
  let type = context.type;

  const funcs = private.gatherFunctions(server, pipeline, method, type);
  if (funcs.length == 0) return Promise.resolve();
  let result;

  for (let func of funcs) {
    let v = vars;
    if (Array.isArray(v))
      v = v.concat(result);
    else
      v.push(result);
    result = await func.apply(context, v);
  }
  return result;
};

// If GET: Type is what is file extension, or accept type.
// If POST: Type is what is being sent in request body/payload.
private.determineType = function(server, req) {

  let ext = "";
  if (req.method.toLowerCase().match(/post|put/)) {
    ext = req.headers["content-type"] || req.headers["Content-Type"];
    return {
      mime: ext,
      type: Object.keys(constants.MIMETypes)
                .find(k => constants.MIMETypes[k] == ext)
    };
  }

  let parts = req.url.split("?")[0].split("/").filter(part => part.length > 0);
  let page = parts.length > 0 ? parts[parts.length - 1] : "";
  let index = page.lastIndexOf(".");
  ext = index < 0 ? "" : page.substr(index + 1);

  if (ext == null || ext.length == 0) {
    let accepttype = req.headers["accept"];
    ext = accepttype.split(",")[0].split("/")[1];
    return {type: ext, mime: accepttype.split(",")[0]};
  }

  ext = ext.replace(".", "").toLowerCase();
  return {type: ext, mime: constants.MIMETypes[ext]};
};

module.exports = server;