const promiser = require("../utils/promiser.js");
const fs = promiser.promisifyNode(require("fs"));
const mustache = promiser.promisifySyncs(require("mustache"));

module.exports = function(s) {

  const settings = s;

  var routes = {"/": {template: "main", page: "home"}};
  var components = {};
  var suppliers = {};

  const find = {
    partials: async(html, searchRoot) => {
      let names = await find.partialNames(html);
      let partials = await Promise.all(
          names.map(
              (partial) => fs.readFileAsync(
                  searchRoot + partial + ".mustache", "utf8")));
      let obj = {};
      names.forEach((name, index) => { obj[name] = partials[index]; });
      return obj;
    },
    partialNames: (html) => new Promise(
                      (resolve, reject) => resolve(
                          (html.match(/{+>.[^{]*}+/g) || [])
                              .map(
                                  (match => match.replace(/{+>/, "").replace(
                                       /}+/, ""))))),
    dataNames:
        (html) => new Promise(
            (resolve, reject) => resolve(
                (html.match(/({{|{{{)[^{>]*(}}|}}})/g) ||
                 [
                 ]).map((match => match.replace(/{+/, "").replace(/}+/, ""))))),
    components: html => find.partials(html, settings.components),
    page: html => find.partials(html, settings.pages),

    supplies: async(req, html, context) => {
      let mapped = {};

      if (routes[this.musty.route.route] == null ||
          routes[this.musty.route.route].supplier == null)
        return mapped;

      // Get supplier for this route:
      let path = `${settings.root}${routes[this.musty.route.route].supplier}`;
      let supplies = {};
      supplies = await require(path).supply(req, this);
      for (let key of Object.keys(supplies)) mapped[key] = supplies[key];

      // Get suppliers from partial names:
      let names = await find.dataNames(html);
      let parts = [];
      for (let i = 0, name = ""; i < names.length; i++) {
        name = names[i];
        parts = name.split(".");
        parts.pop();

        names = names.filter(n => !n.startsWith(parts.join(".")));

        let supp =
            parts.reduce((prev, curr) => { return prev += "/" + curr; }, "");
        if (supp.length == 0) continue;
        path = `${settings.suppliers.root}${supp}.js`;
        try {
          supplies = await require(path).supply(req, this);
          let root = mapped;
          for (let part of parts) {
            if (root[part] == null) {
              root[part] = {};
              root = root[part];
            }
          }
          for (let key of Object.keys(supplies)) root[key] = supplies[key];
        } catch (ex) {
        }
      };
      return mapped;
    }
  };

  let determineRoute = async(req) => {
    // Add data to context so future items can use it.
    this.musty.route = {};

    let url = req.url.path;
    let levels = [];
    let route = "";

    // Try an exact match
    // Only try exact url matches:
    let rs = Object.keys(routes).filter(k => !/{{[^}]*}}/.test(k) && k == url);
    if (rs.length > 0) {
      route = rs[0];
      levels = routes[route].levels;

      // Otherwise, try regex routes and sort by longest starting match:
    } else {
      rs = Object.keys(routes)
               .filter(
                   k => k.split("/").filter(k => k.length > 0).length ==
                       req.url.parts.length)
               .sort((a, b) => {
                 let ai = 0;
                 while (url[ai] == a[ai]) ai++;
                 let bi = 0;
                 while (url[bi] == b[bi]) bi++;
                 return ai < bi;
               });

      if (rs.length > 0) {
        route = rs[0];
        // Map templated literals:
        for (let parts = route.split("/").filter(k => k.length > 0), i = 0;
             i < parts.length; i++)
          if (parts[i].startsWith("{{") && parts[i].endsWith("}}"))
            this.musty.route[parts[i].replace(/{{|}}/g, "")] = req.url.parts[i];

        levels = routes[route].levels;
      }
    }
    this.musty.route.route = route;

    return {levels, route};
  };

  this.fetch = async(req) => {
    this.musty = {};

    let result = await determineRoute(req);
    let levels = result.levels;

    this.musty.levels = levels;

    // Get all level HTML (directly from url structure):
    let tasks = [];
    for (let level of levels)
      tasks.push(fs.readFileAsync(`${settings.root}${level}.mustache`, "utf8"));

    let levelsHtml = await Promise.all(tasks);

    // Get partials found in core html (levels):
    let partialsHtml = [];
    for (let level of levelsHtml) {
      let partialNames =
          (await find.partialNames(level)).filter(p => !p.startsWith("level"));
      for (let name of partialNames)
        partialsHtml.push({
          name,
          html: await fs.readFileAsync(
              `${settings.views}${name.replace(/\./g,"/")}.mustache`, "utf8")
        });
    }

    return {levelsHtml, partialsHtml};

  };

  this.transform = async(req, res, htmlResult) => {
    let partials = {};

    let levels = htmlResult.levelsHtml;
    let supplies = await find.supplies(req, levels[0], this);

    // HTML:
    // Add all level HTML:
    for (let i = 0; i < levels.length; i++) {
      // Add this level of HTML to the partials:
      partials[`level${i}`] = levels[i];
    }
    // Add additional partial HTML detected from levels:
    for (let partialHtml of htmlResult.partialsHtml)
      partials[partialHtml.name] = await partialHtml.html;

    // SUPPLY:
    // Retrieve data for levels:
    for (let i = 0; i < levels.length; i++) {
      let levelSupplies = await find.supplies(req, levels[i], this);
      Object.keys(levelSupplies).forEach(k => supplies[k] = levelSupplies[k]);
    }
    // Retrieve data for partials:
    for (let partialHtml of htmlResult.partialsHtml.map(ph => ph.html)) {
      let partialSupplies = await find.supplies(req, partialHtml, this);
      Object.keys(partialSupplies)
          .forEach(k => supplies[k] = partialSupplies[k]);
    }

    // Add useful server level information:
    supplies.request = req;

    return mustache.renderAsync(levels[0], supplies, partials);
  };

  this.routes = {
    add: (path, ...arguments) => {
      if (arguments.length == 0 && typeof path == "object")
        routes[path.path] = {supplier: path.supplier, levels: path.levels};
      else
        routes[path] = {supplier: arguments.pop(), levels: arguments};
    }
  };
};