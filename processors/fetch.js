const promiser = require("../utils/promiser.js");
const fs = promiser.promisifyNode(require("fs"));
const types = require("../constants.js").MIMETypes;

module.exports = {
  action: function(request, response, result) {
    let url = request.url;
    let file = url.path;
    let encoding = "binary";
    let type = this.mime;

    if (type == null)
      return Promise.reject("Extension not found, or not supported.");
    if (type.startsWith("text/") || type.startsWith("application/"))
      encoding = "utf8";
    if (this.extensionAliases[url.extension])
      file = file.replace(
          "." + url.extension, "." + this.extensionAliases[url.extension]);
    return fs.readFileAsync(this.settings.root + file, encoding);
  }
};