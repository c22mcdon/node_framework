module.exports = function() {
  var actions = {};
  this.add = (action, promise, method) => {
    if (method == null) method = "";
    actions[action + method] = promise;
    return this;
  };
  this.get = (action, promise) => {
    this.add(action, promise, "get");
    return this;
  };
  this.post = (action, promise) => {
    this.add(action, promise, "post");
    return this;
  };
  this.act = async(action, req, res) => {
    let data = {};
    if (req.method.match(/put|post/))
      data = req.body;
    else
      data = req.url.queryString;
    return Promise.resolve(actions[action + req.method](data, req, res));
  }
};