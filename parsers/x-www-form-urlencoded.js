module.exports = {
  action: async function(request) {
    var requestBody = {};
    let data = request.body;
    data.toString().split("&").forEach(values => {
      var key = values.split("=")[0];
      var value = values.split("=")[1];

      if (value != null) {
        if (value.indexOf(".") >= 0 && parseFloat(value))
          value = parseFloat(value);
        else if (parseInt(value))
          value = parseInt(value);
      }

      if (requestBody[key] == null)
        requestBody[key] = value;
      else if (Array.isArray(requestBody[key]))
        requestBody[key].push(value);
      else
        requestBody[key] = new Array(requestBody[key], value);
    });
    request.body = requestBody;
    return requestBody;
  }
};