var promiser = require("../utils/promiser.js");
var mysql = promiser.promisifyNode(require("mysql"));

var db = function(dbSettings) {
  var logger = dbSettings.logger;
  var connPool = mysql.createPool(dbSettings);
  this.query = function(q) {
    var conn = null;
    return new Promise(function(resolve, reject) {
             connPool.getConnection((err, c) => {
               if (err != null) return reject(err);
               conn = c;
               conn.query(q, (err, results) => {
                 if (err != null) return reject(err);
                 return resolve(results);
               });
             })
           })
        .then(results => {
          conn.release();
          return results;
        })
        .catch(err => {
          if (logger != null) logger.log(err);
          if (conn != null) conn.release();
        });

  };

  this.table = (name) => new Table(name, this);
  this.view = (name) => new Table(name, this);
};

function Table(name, db) {
  this.name = name;

  var extract = {
    fields: function(obj) { return Object.keys(obj); },
    values: function(obj) {
      let values = [];
      let temp = "";
      let keys = Object.keys(obj);
      for (let i = 0; i < keys.length; i++) {
        temp = obj[keys[i]];
        if (typeof temp == "string" || temp instanceof String)
          temp = "'" + temp + "'";
        values.push(temp);
      };
      return values;
    }
  };

  this.update = (filterObj, setObj) => {
    let uFields = extract.fields(setObj);
    let uValues = extract.values(setObj);
    let set = new Array();
    for (let i = 0; i < uFields.length; i++)
      set.push(`${uFields[i]} = ${uValues[i]}`);

    let fFields = extract.fields(filterObj);
    let fValues = extract.values(filterObj);
    let fFilter = new Array();
    for (let i = 0; i < fFields.length; i++)
      fFilter.push(`${fFields[i]} = ${fValues[i]}`);

    return db.query(
        `update ${this.name} set ${set.join(",")} where ${fFilter.join(" and ")}`);
  };

  this.insert = async(obj, IDName) => {
    let result = await db.query(
        `insert into ${this.name}(${extract.fields(obj).join(",")}) values(${extract.values(obj).join(",")})`);
    return result.insertId;
  };
  this.select = (projection, filterObj) => {
    let fFields = extract.fields(filterObj);
    let fValues = extract.values(filterObj);
    let fFilter = new Array();
    for (let i = 0; i < fFields.length; i++)
      fFilter.push(`${fFields[i]} = ${fValues[i]}`);

    let proj = Array.isArray(projection) ? projection.join(",") : projection;
    return db.query(
        `select ${proj} from ${this.name} where ${fFilter.join(" and ")}`)
  };

  this.exists = (obj) => {
    let fields = extract.fields(obj);
    let values = extract.values(obj);
    let filter = new Array();

    for (let i = 0; i < fields.length; i++)
      filter.push(`${fields[i]} = ${values[i]}`);
    return db.query(`select * from ${this.name} where ${filter.join(" and ")}`)
        .then(
            (results) =>
                results != null && results.length > 0 ? results : false);
  };

  this.if = (condition, prom) => {
    return Promise.resolve(condition).then((result) => {
      if (result) return prom;
    });
  };

  this.ifelse = (condition, successProm, failProm) => {
    return Promise.resolve(condition).then(
        result => { return result ? successProm(result) : failProm(result); })
  };
};

module.exports = db;
