module.exports = {
  invalidURL: (req) => { return req.url.match(/[<>\'\";,]/g); },
  action: (req) => {
    if (module.exports.invalidURL(req)) {
      let err = new Error("Potential Attack Detected!");
      err.statusCode = 403;
      throw err;
    }
  }
  // check for SQL injection too:
};