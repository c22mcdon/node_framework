module.exports = {
  action: function(req) {
    let reqBody = [];
    return new Promise((resolve, reject) => req.on("data", (chunk) => {
                                                 reqBody.push(chunk);
                                               }).on("end", () => {
      req.body = reqBody;
      return resolve(reqBody);
    }));
  }
};