module.exports = {
  action: async(req, res) => {
    res.cookies = {
      add: (name, value, opts) => {
        if (this.cookies == null) this.cookies = {};
        this.cookies[name] = {
          value,
          http: opts.http || true,
          secure: opts.secure || true,
          expires: opts.expires || null,
          domain: opts.domain || null,
          path: opts.path || null
        };
      },
      remove: (name) => {
        if (this.cookies == null) return;
        delete this.cookies[name];
      },
      get: (name) => {
        if (this.cookies == null) return null;
        return this.cookies[name];
      },
      empty: () => Object.keys(this.cookies).length == 0,
      names: () => Object.keys(this.cookies),
      write: (name) => {
        if (this.cookies == null || this.cookies[name] == null) return "";
        const cookie = this.cookies[name];
        return `
          ${name}=${cookie.value}; 
          ${cookie.http ? "HttpOnly;" : ""} 
          ${cookie.secure ? "Secure;" : ""} 
          ${cookie.expires ? "Expires=" + cookie.expires.toString() + ";" : ""}
          ${cookie.domain ? "Domain=" + cookie.domain + ";" : ""}
          ${cookie.path ? "Path=" + cookie.path + ";" : ""}
        `;
      }
    }
  }
};