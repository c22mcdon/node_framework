module.exports = {
  action: async function(req) {
    req.contentType = req.headers["content-type"];
    req.method = req.method.toLowerCase();
    req.userAgent = req.headers["user-agent"];
    req.origin = req.headers["origin"];
    req.cookies = typeof req.headers["cookie"] == "string" ?
        req.headers["cookie"].split(";").reduce(
            (obj, cookie) => {
              let vals = cookie.split("&");
              obj[vals[0]] = vals[1];
            },
            {}) :
        {};
    req.accept = {
      types: req.headers["accept"] != null ? req.headers["accept"].split(",") :
                                             "",
      language: req.headers["accept-language"] != null ?
          req.headers["accept-language"].split(",") :
          "",
      charset: req.headers["accept-charset"],
      encoding: req.headers["accept-encoding"]
    };

    req.port = req.connection.localPort;
    req.host =
        req.headers["host"] != null ? req.headers["host"].split(":")[0] : "";

    req.url = {
      url: req.url,
      proto: req.port == 443 ? "https" : "http",
      domain: req.host,
      path: req.url.indexOf("?") >= 0 ?
          req.url.split("?")[0] :
          req.url,  // everything but the query string
      qs: req.url.indexOf("?") >= 0 ? req.url.split("?")[1] : "",
      parts: req.url.split("?")[0].split("/").filter(part => part.length > 0)
    };
    if (req.url.parts.length == 0) req.url.parts.push("/");
    req.url.full = `${req.url.proto}://${req.url.domain}${req.url.path}`;

    req.url.page = req.url.parts[req.url.parts.length - 1];
    req.url.queryString = req.url.qs.length > 0 ?
        req.url.qs.split("&").reduce(
            (obj, kv) => {
              let vals = kv.split("=");
              let key = vals[0];
              let value = vals[1];
              if (Array.isArray(obj[key]))
                obj[key].push(value);
              else if (obj[key] != null)
                obj[key] = new Array(obj[key], value);
              else
                obj[key] = value;
              return obj;
            },
            {}) :
        {};
    Object.keys(req.url).forEach(key => {
      if (typeof req.url[key] == "string")
        req.url[key] = decodeURI(req.url[key]);
      if (Array.isArray(req.url[key]))
        for (let i = 0; i < req.url[key].length; i++)
          if (typeof req.url[key][i] == "string")
            req.url[key][i] = decodeURI(req.url[key][i])
    });
    Object.keys(req.url.queryString).forEach(key => {
      if (typeof req.url.queryString[key] == "string")
        req.url.queryString[key] = decodeURI(req.url.queryString[key]);
    });

    let index = req.url.page.lastIndexOf(".");
    let ext = index < 0 ? "" : req.url.page.substr(index + 1);
    req.url.extension = ext;

    req.clientIP = typeof req.headers["x-forwarded-for"] == "string" ?
        req.headers["x-forwarded-for"].split(",")[0] :
        typeof req.headers["forwarded"] == "string" ?
        req.headers["forwarded"]
                .split(";")
                .filter(h => h.startsWith("for"))[0]
                .split("=")[1] :
        req.connection.remoteAddress;

    req.clientProto = typeof req.headers["x-forwarded-proto"] == "string" ?
        req.headers["x-forwarded-proto"].split(",")[0] :
        typeof req.headers["forwarded"] == "string" ?
        req.headers["forwarded"]
                .split(";")
                .filter(h => h.startsWith("proto"))[0]
                .split("=")[1] :
        req.url.proto;

    req.is = {
      local: req.clientIP.endsWith("127.0.0.1"),
      https: req.clientProto == "https",
      ajax: typeof req.headers["X-Ajax"] == "string"
    };

    return req;
  }
};